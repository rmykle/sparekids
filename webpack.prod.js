const common = require('./webpack.config')
const merge = require('webpack')
const Uglify = require('uglifyjs-webpack-plugin');
const webpack = require('webpack');


module.exports = merge(common, {
    plugins: [
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify('production')
          }),
          new webpack.optimize.UglifyJsPlugin()
    ]
});