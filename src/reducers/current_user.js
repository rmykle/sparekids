import { CHANGE_USER, LOAD_OFFLINE } from "../actions/index";


export default (state = null, action) => {

    switch(action.type) {
        case CHANGE_USER:
            return action.payload;
        default: return state;
    }
}