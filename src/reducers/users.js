import { CHANGE_USER, LOAD_OFFLINE, ADD_TASK, REMOVE_TASK, CONFIRM_TASK, MAKE_A_WISH, COMPLETE_TASK } from "../actions/index";
import update from 'immutability-helper';


export default(state={}, action) => {
    switch(action.type) {
        case ADD_TASK:
            return update(state, {
                [action.payload.child]: {tasks: {$push: [action.payload.task]}}
            });
            return newTask;

        case REMOVE_TASK:
            return update(state, {
                [action.payload.child]: {tasks: items => items.filter(task => task.id != action.payload.removeTask)}
            })

        case CONFIRM_TASK:
            return update(state, {
                [action.payload.child]: {tasks: items => items.filter(task => task.id != action.payload.task.id),
                account: acc => acc + action.payload.task.cash
                }
            })

        case COMPLETE_TASK:
            return update(state, {
                [action.payload.childID]: {tasks: tasks => tasks.map(task => {
                    if(task.id == action.payload.taskID) return {...task, isPending: true}
                    return task;
                })}
            })

        case MAKE_A_WISH:
            return update(state, {
                [action.payload.child]: {wishes: {$push: [action.payload.wish]}}
            })

        case LOAD_OFFLINE:
            return action.payload;

        default: 
            return state;
    }
}