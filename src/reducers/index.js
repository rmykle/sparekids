import {combineReducers} from 'redux';
import users from './users';
import currentUser from './current_user';


export default combineReducers({
    users, currentUser
});

