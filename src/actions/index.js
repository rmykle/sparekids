
export const LOAD_OFFLINE = "LOAD_OFFLINE";
export const CHANGE_USER = "CHANGE_USER";

export const MAKE_A_WISH = "MAKE_A_WISH";

export const ADD_TASK = "ADD_TASK";
export const REMOVE_TASK = "REMOVE_TASK";
export const CONFIRM_TASK = "CONFIRM_TASK";
export const COMPLETE_TASK ="COMPLETE_TASK";

import axios from 'axios';

export const loadOffline = () => {
    const request = axios.get("offline.json");
    return dispatch => {
        request.then(({data}) => {
            dispatch({type: LOAD_OFFLINE, payload: data})
        })
    }
}

export const changeUser = user => {
    return {type: CHANGE_USER, payload:user}
}

export const addTask = (task, child) => {
    return {
        type: ADD_TASK, 
        payload: ({task,child})
    }
}

export const removeTask = (task, child) => {
    return {
        type: REMOVE_TASK, 
        payload: ({removeTask: task.id, child})
    }
}

export const confirmTask = (task, child) => {
    return {
        type: CONFIRM_TASK, 
        payload: ({task, child})
    }
}
export const completeTask = (task, child) => {
    return {
        type: COMPLETE_TASK,
        payload: {taskID: task.id, childID: child}
    }
}

export const makeAWish = (wish, child) => {
    return {
        type: MAKE_A_WISH,
        payload:({wish,child})
    }
}