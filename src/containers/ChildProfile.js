import React, { Component } from 'react';
import {connect} from 'react-redux';
import Badges from '../components/Badges';
import FooterMenu from '../components/FooterMenu';
import ChildProfileHeader from '../components/ChildProfileHeader';
import SubTitle from '../components/SubTitle';
import {completeTask} from '../actions';
import ChildTask from '../components/ChildTask';
import WishList from '../components/WishList';
import NoTaskWarning from '../components/NoTaskWarning';

export const TASKS_SELECTED = "Tasks";
export const SAVINGS_SELECTED = "Goals";

class ChildProfile extends Component  {
    constructor(props) {
        super(props);
        this.state = {selectedWindow: TASKS_SELECTED};
        this.onWindowSelect = this.onWindowSelect.bind(this);
    }
    render() {
       
        const currentUser = this.props.users[this.props.currentUser];

        return (
            <div>
                <div className="cProfileWrap">
                    <div className="profile">
                        <ChildProfileHeader child={currentUser}/>
                        {this.state.selectedWindow == TASKS_SELECTED 
                            ? this.taskList(currentUser) 
                            : <WishList wishes={currentUser.wishes} account={currentUser.account}/>}
                    </div>
                    
                </div>
                <FooterMenu onWindowSelect={this.onWindowSelect} selected={this.state.selectedWindow}/>
            </div>
        )
    }

    taskList(currentUser) {
        const {tasks} = currentUser;
        return (
            <div>
                <SubTitle text={TASKS_SELECTED}/>
                <ul>
                    {tasks.length != 0 ? tasks
                    .sort((a,b) => !a.isPending ? -1 : a.cash > b.cash ? 0 : 1)
                    .map(task => {
                        return <ChildTask key={task.id} task={task} onComplete={(task) => this.onComplete(task)}/>
                    }): <NoTaskWarning text="No tasks"/>}
                </ul>

                <SubTitle text="Achievements"/>
                <Badges/>     
            </div>
            

        )
    }

    onWindowSelect(selected) {
        this.setState({selectedWindow: selected});
    }

    onComplete(task){
        this.props.completeTask(task, this.props.currentUser);
    }
}

const mapStateToProps = state => {

    return {
        users: state.users,
        currentUser: state.currentUser
    }
}

export default connect(mapStateToProps, {completeTask})(ChildProfile);