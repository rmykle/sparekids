import React, { Component } from 'react';
import {connect} from 'react-redux';
import {addChild} from '../actions';
import ChildProfileHeader from '../components/ChildProfileHeader';
import SubTitle from '../components/SubTitle';
import {addTask, removeTask, confirmTask, makeAWish} from '../actions';
import Modal from '../components/Modal';
import _ from 'lodash';
import WishList from '../components/WishList';
import ObjectiveCreator from '../components/ObjectiveCreator';
import ParentTask from '../components/ParentTask';
import NoTaskWarning from '../components/NoTaskWarning';

class ParentProfile extends Component {
    constructor(props){
        super(props);
        
        const firstChild = Object.values(this.props.users).filter(user => !user.isParent)[1];
        const presetTasks =  [{title: "Walk the dog", cash: 20}, {title: "Clean room", cash: 30}];
        const presetWishes = [{title: "Bike", cash: 2999}];
        this.state = {selectedChild: firstChild.id, showNewTask: false, showNewWish:false, modalTask:undefined, nextID: 7, presetTasks: presetTasks, presetWishes: presetWishes};
        
        this.removeTask = this.removeTask.bind(this);
        this.confirmTask = this.confirmTask.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.addTask = this.addTask.bind(this);
        this.closeNewTask = this.closeNewTask.bind(this);
    }
    


    render() {
        const selectedChild = this.props.users[this.state.selectedChild];
        const wishPreset = this.state.showNewWish ? this.state.presetWishes.pop() : null;
        return (
            <div>
                 {this.childrenList(selectedChild)}

                <div className="profile">
                    <ChildProfileHeader child={selectedChild}/>

                    <SubTitle text="Tasks" onClick={() => this.setState({showNewTask:true})}/>
                    {this.taskList(selectedChild)}

                    <SubTitle text="Goals" onClick={() => this.setState({showNewWish: true})}/>
                    <WishList 
                    preset={wishPreset}
                    onSubmit={this.state.showNewWish ? wish => this.makeAWish(wish) : null}
                    onClose={() => this.setState({showNewWish: false})}
                    wishes={selectedChild.wishes} 
                    account={selectedChild.account}/>

                    {this.state.modalTask ? <Modal task={this.state.modalTask} performedBy={selectedChild.name} 
                    confirm={this.confirmTask} close={this.closeModal}/> : ""}
                </div>
            </div>
        )
    }

    childrenList(selectedChild) {
        const children= _.values(this.props.users)
        .sort((a,b) => a.name > b.name)
        .filter(user => !user.isParent);
        return (
            <ul className="childList">
                {
                    children.map((child) => {
                        return (
                            <li key={child.name}
                                className={child == selectedChild ? "selectedChildIcon" : ""}
                                onClick={() => this.setState({selectedChild: child.id})}>
                                <img src={`img/${child.img ? child.img : "profile.png"}`}/>
                                <p>{child.name.split(" ")[0]}</p>
                            </li>
                        )
                    })
                }
        </ul>
        )
    }

    taskList(selectedChild) {
        const {tasks} = selectedChild;
        const nextPreset = this.state.showNewTask ? this.state.presetTasks.pop() : null;
        return (
            <ul>
                {this.state.showNewTask ? <ObjectiveCreator preset={nextPreset} tasks={true}onCreate={this.addTask} onClose={this.closeNewTask}/> : ""}
                {tasks.length != 0 ?
                tasks.sort((a,b) => a.isPending  ? 0 : a.cash > b.cash ? 1 : 2)
                .map((task, index) => {
                    return <ParentTask key={index} task={task} onDelete={this.removeTask} onConfirm={this.confirmTask}/>
                }) : <NoTaskWarning text="No tasks"/>}
            </ul>
        )
    }

    makeAWish(wish){
        const newWish = {id: this.state.nextID, name: wish.title, cost: wish.cash}
        this.props.makeAWish(newWish, this.state.selectedChild);
        this.setState({showNewWish:false, nextID: this.state.nextID+1})
    }

    removeTask(task) {
        this.props.removeTask(task, this.state.selectedChild);
    }

    confirmTask(task, fromModal) {
        if(fromModal) this.props.confirmTask(task, this.state.selectedChild);
        else this.setState({modalTask: task})
    }

    closeModal() {
        this.setState({modalTask: undefined});
    }


    addTask(task) {
        task.id = this.state.nextID;
       this.props.addTask(task, this.state.selectedChild);
       this.setState({nextID: this.state.nextID +1})
       this.closeNewTask();
    }

    closeNewTask() {
        this.setState({showNewTask: false})
    }

}

const mapStateToProps = state => {
    return {
        users: state.users
    };
}

export default connect(mapStateToProps, {addTask, removeTask, confirmTask, makeAWish})(ParentProfile);