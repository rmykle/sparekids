import React, { Component } from 'react';
import "../css/style.css";
import bulma from 'bulma';
import {connect} from 'react-redux';
import { changeUser, loadOffline } from '../actions/';
import ChildProfile from './ChildProfile';
import FooterMenu from '../components/FooterMenu';
import ParentProfile from './ParentProfile';
import DemoPanel from '../components/DemoPanel';

class App extends Component {
    constructor(props){
        super(props);
    }

    componentDidMount() {
        this.props.loadOffline();
    }

    render() { 
        const currentUser = this.props.users[this.props.currentUser];  

        if(!currentUser)return <DemoPanel users={this.props.users} changeUser={this.props.changeUser}/>
      
        return (
            <div>
                <nav >
                    <div><img src="img/logo.png" onClick={() => this.props.changeUser("")}/></div>
                </nav>
                {currentUser.isParent ? <ParentProfile/> : <ChildProfile/>}
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    users: state.users,
    currentUser: state.currentUser
})

export default connect(mapStateToProps, {changeUser, loadOffline})(App);