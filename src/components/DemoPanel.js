import React from 'react';

const DemoPanel = ({users, changeUser}) => {
    return (
        <div className="demo">
            <img src="img/logo.png"/>
            <h2 className="title is-2">Demopanel</h2>
            <ul className="userList">
                {_.map(users, user =>(createIcon(user, changeUser)))}
            </ul>
        </div>
    );
};

const createIcon = (user, changeUser) => {
    const image = `img/${user.img ? user.img : "profile.png"}`;
    return (
        <li
            key={user.name}
            onClick={() => changeUser(user.id)}
        >
            <img src={image}/>
            {user.name}
        </li>
    )
}

export default DemoPanel;