import React from 'react';
import ObjectiveCreator from './ObjectiveCreator';

export default ({wishes, account, onSubmit, onClose, preset}) => {

    return (
        <ul>
            {onSubmit ? <ObjectiveCreator preset={preset} onCreate={onSubmit} onClose={onClose}/> : null}
            {wishes
            .sort((a,b) => a.cost - b.cost)
            .map(wish => {
                return (
                    <li key={wish.name} className="wish">
                    <p>{`${wish.cost} kroner`}</p>
                    <p>{account >= wish.cost ? <img  src="img/savingdone.png"/>
                    : <img src="img/savingincomplete.png"/>}</p>
                <p>{wish.name}</p>
            </li>
                )
            })}
        </ul>
    )
}