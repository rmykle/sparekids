import React from 'react';

export default ({task, onConfirm, onDelete}) => {
    const {isPending} = task;
    return (
        <li className="roundComponent task">
            <div onClick={() => onDelete(task)}>
                    <img className="taskIcon" src="img/premovetask.png"/>
            </div>
            <div>
                <p>{`${task.title} (${task.cash} kroner)`}</p>
                <p>{isPending
                        ? "Pending"
                        : "Incomplete"
                    }
                </p>
            </div>
            <div 
                className={`${isPending ? "taskPending" : ""}`}
                onClick={() => {
                    if(isPending) onConfirm(task);
                }}>
                <img className="taskIcon" src={`img/${isPending ? "ptaskdone.png" : "timeglass.png"}`}/>
            </div>
        </li>
    )
}