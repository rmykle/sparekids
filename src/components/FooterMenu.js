import React from 'react';
import { TASKS_SELECTED, SAVINGS_SELECTED } from '../containers/ChildProfile';

export default ({onWindowSelect, selected}) => {
    return (
        <footer>
            {footerItem(TASKS_SELECTED, selected, onWindowSelect)}
            {footerItem(SAVINGS_SELECTED, selected, onWindowSelect)}
        </footer>
    )

}

const footerItem = (name, selected, onWindowSelect) => {
    return(
        <div 
            className={name == selected ? "selectedFooterItem" : ""}
            onClick={() => onWindowSelect(name)}>
            {name}
        </div>
    )
}
