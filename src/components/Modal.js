import React from 'react';

export default ({task, performedBy, close, confirm}) => {
    const date = new Date();
    return (
    <div className="modal is-active">
    <div className="modal-background"></div>
    <div className="modal-content">
      <div className="paymentModal roundComponent">
          <p><strong>{task.title}</strong></p>
          <p>{`Completed by ${performedBy}`}</p>
          <p>{`${date.toLocaleDateString()}`}</p>
          <p>{`Would you like to transfer ${task ? task.cash : 0} kroner?`}</p>
            <div>
              <p className="modalButton" onClick={() => close()}>Reject</p>
              <p className="modalButton" onClick={() => {
                  confirm(task, true)
                  close();
                  }}>Accept</p>
          </div>

          </div>
    </div>
    <button className="modal-close is-large" onClick={() => close()}></button>
  </div>
    )
}