import React from 'react';

export default ({text, onClick, savings}) => {
    return (
        <div className="subTitleWrap">
            <div className="subTitle radius">
                <p>{text}</p>
                {onClick ? <p className="plus" onClick={() => onClick(true)}>+</p> : ""}
            </div>
        </div>
    )
}