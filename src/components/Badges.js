import React from 'react';

export default () => {
    const samples = ["badge1.png","badge2.png","badge3.png","badge4.png","badge5.png"];
    let index = 0; 
    return (

            <ul className="badgeList radius">
                {samples.map((badge, index) => createBadge(badge, index))}
            </ul>
    )
}

const createBadge = (badge, index) => {
    return (
        <li key={index}> <img src={`img/${badge}`}/></li>
    )
}