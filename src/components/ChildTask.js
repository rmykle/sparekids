import React from 'react';

export default ({task, onComplete}) => {
    return (
        <li className="roundComponent task">
            <div></div>
            <div>
                <p>{`${task.title}`} </p>
                <p>{task.isPending ? `Pending ${task.cash} kroner` : <br/>}</p>
            </div>
            <div>
                {!task.isPending 
                    ? <div className="taskcoin" onClick={() => onComplete(task)}>
                        <p>{task.cash}</p>
                        <img className="coin" src="img/coin.png"/>
                    </div> 
                    : <div className="taskcoin"><img className="check" src="img/checkcoin.png"/></div>}
            </div>
        </li>
    )
}