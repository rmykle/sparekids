import React, { Component } from 'react';

class TaskCreator extends Component {
    constructor(props) {
        super(props);


            const {preset} = this.props;
            
            const state = preset ? {title: preset.title, cash: preset.cash} : {title:"", cash: 10};
            this.state = state;
        

    }
    render() {

        return (
            <li className="roundComponent newTask">
                <div>
                    <div><img src="img/premovetask.png" onClick={() => this.closeTask()}/></div>
                        <input value={this.state.title} onChange={(e) => this.setState({title: e.target.value})}/>
                    <div><img src="img/ptaskdone.png" onClick={() => this.createTask()}/></div>
                </div>
                <div className="setPrice">
                    <img src="img/minus.png" onClick={() => {
                        const {cash} = this.state;
                        if(cash > 0) this.setState({cash: cash-1})
                    }}/>
                    <div className="priceInput">
                        <input value={this.state.cash} onChange={e => this.onPriceChange(e)} />
                        <p>kroner</p>
                    </div>
                    <img src="img/plus.png" onClick={() => this.setState({cash: this.state.cash + 1})}/>
                </div>
            </li>
        )
    }

    onPriceChange(e) {
        const {value} = e.target;
        if(/^\d+$/.test(value) || value=="") {
            this.setState({cash: value});
        }
    }

    createTask() {
        if(!this.state.title) return;
        const task = {title:this.state.title, cash: Number(this.state.cash)};
        this.reset();
        this.props.onCreate(task);       

    }

    closeTask() {
        this.reset();
        this.props.onClose();
    }

    reset() {
        this.setState({title: "", cash: 10})
    }
}

export default TaskCreator;