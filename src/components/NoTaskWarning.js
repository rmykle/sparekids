import React from 'react';

export default ({text}) => {
    return <div className="warning"><i>{text}</i></div>
}