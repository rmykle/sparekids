import React from 'react';

export default ({child}) => {
    return (
        <div className="childProfileHeader roundComponent">
            <img src={`img/${child.img ? child.img : "profile.png"}`}/>
            <p><strong>{child.name}</strong></p>
            <p>{`Balance: ${child.account} kr`}</p>
        </div>
    )
}